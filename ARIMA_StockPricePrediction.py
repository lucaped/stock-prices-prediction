#!/usr/bin/env python
# coding: utf-8

# Stock prices prediction using AutoRegressive Integrated Moving Average (ARIMA) model 
# Author: Luca Pedrelli

# %% imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec
from datetime import datetime

import tensorflow as tf
from function import check_stationary, model_performance
from statsmodels.tsa.arima_model import ARIMA
from pmdarima.arima import auto_arima

tf.test.gpu_device_name()

# %% Load and plot the data

dateparse = lambda x: datetime.strptime(x, '%Y-%m-%d')
data = pd.read_csv('AABA.csv', sep=",", parse_dates=['Date'], date_parser=dateparse)

# remove Nan and Inf values if present in the dataset
data.replace([np.inf, -np.inf], np.nan)
data = data.dropna()

print('The dataset has ' + str(len(data)) + ' records')
print('The dataset contains data from ' + data.iloc[0]['Date'].strftime("%d/%m/%Y") + ' to ' + data.iloc[-1]['Date'].strftime("%d/%m/%Y"))

# Plot the data
plt.figure()
plt.plot(data['Date'],data['Close'], 'b', label='Closing pirce')
plt.xlabel('Date')
plt.ylabel('Close Price')
plt.title('Altaba Inc. stock price')
plt.legend()

# %% Check for degree of stationarity of the time series
# STATIONARY time series: if they do not have trend or seasonal effect, i.e. if the statistical properties of the time series do not change over time.  
# Statistical modeling methods, in order to be effective, require the time series to be stationary.
# 
# AUGMENTED DICKEY-FULLER (ADF) test is a type of statistical test called unit root test to test the stationarity of the time series.
# Interpretation of the ADF test results:
# p-value > 5% (1%) fail to reject the null hypothesis, the data has a unit root and is non-stationary
# p-value < 5% (1%) reject the null hypothesis, the data does not have a unit root and is stationary
# ADF statistic: the more negative the more likely we are to reject the null hypothesis (we have a stationary dataset)
# 
# NOTE: The ARIMA model converts non-stationary data to stationary data before working on it!!! 
# The analysis conducted here is intended to give an overview how to check and make a time series stationary. 

check_stationary(data['Close'], data['Date']) 

# %% Make time series stationary

# Different strategies can be used to make a time series stationary. Here I consider two possible options:
# 1) use the weighted rolling mean and subtract it from the original data, and
# 2) differencing

# %% 1) weighted rolling mean

w_rolling_mean = data['Close'].ewm(alpha=0.01).mean()
data_stationary = data['Close'] - w_rolling_mean

# apply the ADT test to check the stationarity of the transformed time series
check_stationary(data_stationary, data['Date'])

# %% 2) differencing

data_diff = data['Close'].diff().fillna(data['Close'])

# apply the ADT test to check the stationarity of the transformed time series
check_stationary(data_diff, data['Date'])


# %% Data pre-processing and train-test split

# We consider the log of the data series in order to reduce the rising trend of the series
X = np.log(data['Close']).to_numpy()

# train-test split
train_size = 0.9
size = int(len(X)*train_size)
train, test = X[0:size], X[size:len(X)]
train_date, test_date = data['Date'][0:size], data['Date'][size:len(X)]

plt.figure()
plt.plot(train_date, np.exp(train), 'r', label='Train set')
plt.plot(test_date, np.exp(test), 'b', label='Test set')
plt.ylabel('Close Price')
plt.xlabel('Date')
plt.title('Altaba Inc. stock price')
plt.legend() 

print('The train set corresponds to the ' + str(train_size*100) + '% of the entire dataset')
print('We will forecast the stock prices from ' + test_date.iloc[0].strftime("%d/%m/%Y") + ' till ' + test_date.iloc[-1].strftime("%d/%m/%Y"))


# %% ARIMA model
# ARIMA (AutoRegressive Integrated Moving Average)
# We will use **auto_arima** to find the optimal order parameters (p, d and q) for an ARIMA model. Where:
# - p is the order of the AR model, i.e. the number of lag observations,
# - d is the degree of differencing needed for stationarity, and,
# - q is the number of lagged forecast errors in the prediction equation.
# 
# The auto_arima function seeks to identify the most optimal parameters for the ARIMA model in a grid search fashion. 
# It works by conducting differencing test to determine the order of differencing, d, and then fitting models within ranges of defined start_p, max_p, start_q, max_q ranges.
# If the seasonal optional is enabled, auto_arima also seeks to identify the optimal P and Q hyper-parameters to determine the optimal order of seasonal differencing, D.

model = auto_arima(train, 
                   start_p=0, start_q=0,  # start valus of p and q
                   test='adf',            # use adftest to find optimal 'd'
                   max_p=5,   max_q=5,    # maximum p and q
                   m=1,                   # frequency of series
                   d=None,                # let model determine d
                   max_d = 3,             # max value of d
                   seasonal=False,        # No Seasonality
                   start_P=0, 
                   D=0, 
                   trace=True,
                   error_action='ignore',  
                   suppress_warnings=True, 
                   stepwise=True)

print(model.summary())


# %% ARIMA model

# %% One step forecast using the ARIMA model.
# -----------------------------------------------------------------------------
# The ARIMA model forecasts the next stock price value using the earlier price observations.
# In this case, the ARIMA prediction for the stock price at time (t+1) is not used to predict the stock price at time (t+2). 
# Instead, the real stock price observation at time (t+1) is used.


history = [x for x in train]
prediction_one_step = list()
for t in range(len(test)): 
    arima_model = ARIMA(history, order=(3,1,0))
    model_fit = arima_model.fit(disp=0)
    output = model_fit.forecast()
    yhat = output[0]
    prediction_one_step.append(yhat)
    obs = test[t]
    # I append as next point the real observation of the stock price
    history.append(obs)

# %% plot the results
plt.figure(figsize=(15, 5))
gs = gridspec.GridSpec(1, 2, width_ratios=[2.5, 1.5]) 
ax0 = plt.subplot(gs[0])
plt.plot(train_date, np.exp(train), '.-r', label = 'Train set')
plt.plot(test_date, np.exp(test), '.-b', label = 'Test set')
plt.plot(test_date, np.exp(prediction_one_step), 'g', label = 'ARIMA model prediction')
plt.ylabel('Close Price', fontsize=14)
plt.xlabel('Date', fontsize=14)
plt.xticks(rotation=45, fontsize=12)
plt.yticks(fontsize=12)
plt.title('Altaba Inc. stock price - Overview', fontsize=14)
plt.legend(fontsize=14)
ax0 = plt.subplot(gs[1])
plt.plot(test_date, np.exp(test), '.-b', label = 'Test set')
plt.plot(test_date, np.exp(prediction_one_step), 'g', label = 'ARIMA model prediction')
plt.ylabel('Close Price', fontsize=14)
plt.xlabel('Date', fontsize=14)
plt.xticks(rotation=45, fontsize=12)
plt.yticks(fontsize=12)
plt.title('Test set and prediction', fontsize=14)
plt.legend(fontsize=14)

print('One step ARIMA model performance: ')
model_performance(np.exp(test), np.exp(prediction_one_step))

# %% Multi step forecast using the ARIMA model.
# -----------------------------------------------------------------------------
# The real challange is not to predict only one stock price ahead but multiple stock prices in the future.
# In this case, the ARIMA prediction for the stock price at time (t+1) is used to predict the stock price at time (t+2).

history = [x for x in train]
prediction_multi_step = list()
for t in range(len(test)): 
    arima_model = ARIMA(history, order=(3,1,0))
    model_fit = arima_model.fit(disp=0)
    output = model_fit.forecast(1, alpha=0.05)
    yhat = output[0]
    prediction_multi_step.append(yhat)
    obs = test[t]
    # NOTE: I append as next point the prediction made by the ARIMA model
    history.append(yhat)

# %% plot the results
plt.figure(figsize=(15, 5))
gs = gridspec.GridSpec(1, 2, width_ratios=[2.5, 1.5]) 
ax0 = plt.subplot(gs[0])
plt.plot(train_date, np.exp(train), '.-r', label = 'Train set')
plt.plot(test_date, np.exp(test), '.-b', label = 'Test set')
plt.plot(test_date, np.exp(prediction_multi_step), 'g', label = 'ARIMA model prediction')
plt.ylabel('Close Price', fontsize=14)
plt.xlabel('Date', fontsize=14)
plt.xticks(rotation=45, fontsize=12)
plt.yticks(fontsize=12)
plt.title('Altaba Inc. stock price - Overview', fontsize=14)
plt.legend(fontsize=14)
ax0 = plt.subplot(gs[1])
plt.plot(test_date, np.exp(test), '.-b', label = 'Test set')
plt.plot(test_date, np.exp(prediction_multi_step), 'g', label = 'ARIMA model prediction')
plt.ylabel('Close Price', fontsize=14)
plt.xlabel('Date', fontsize=14)
plt.xticks(rotation=45, fontsize=12)
plt.yticks(fontsize=12)
plt.title('Test set and prediction', fontsize=14)
plt.legend(fontsize=14)

print('Multi step ARIMA model performance: ')
model_performance(np.exp(test), np.exp(prediction_multi_step))

