#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 10:55:20 2020

@author: luca
"""
import matplotlib.pyplot as plt
import numpy as np
import math
from statsmodels.tsa.stattools import adfuller
from sklearn.metrics import mean_squared_error, mean_absolute_error

def check_stationary(data, x_axis):
    # STATIONARY time series: if they do not have trend or seasonal effect
    # statistical modeling methods, in order to be effective, require the time series to be stationary
    # AUGMENTED DICKEY-FULLER (ADF) test is a type of statistical test called unit root test
    # interpretation of the results:
    # p-value > 5% (1%) fail to reject the null hypothesis, the data has a unit root and is non-stationary
    # p-value < 5% (1%) reject the null hypothesis, the data does not have a unit root and is stationary
    # ADF statistic: the more negative the more likely we are to reject the null hypothesis (we have stationery dataset)

    adf_test = adfuller(data)
    print('ADF Statistic: %f' % adf_test[0])
    print('p-value: %f' % adf_test[1])
    print('Critical Values:')
    for key, value in adf_test[4].items():
    	print('\t%s: %.3f' % (key, value))
        
    # statistical plots
    # check mean and stabdard deviation. If both are flat lines, the time series become stationary
    rolling_mean = data.rolling(window=90).mean()
    rolling_std = data.rolling(window=90).std()
    
    plt.figure()
    plt.plot(x_axis,data, 'b', label='Closing pirce')
    plt.plot(x_axis,rolling_mean, 'r', label='Rolling mean')
    plt.plot(x_axis,rolling_std, 'k', label='Rolling standard dev.')
    plt.xlabel('Date')
    plt.ylabel('Close Price')
    plt.title('Altaba Inc. stock price')
    plt.legend()  
    
def model_performance(test, prediction):
    mse = mean_squared_error(test, prediction)
    print('MSE: '+str(mse))
    mae = mean_absolute_error(test, prediction)
    print('MAE: '+str(mae))
    rmse = math.sqrt(mean_squared_error(test, prediction))
    print('RMSE: '+str(rmse))
    mape = np.mean(np.abs(prediction - test)/np.abs(test))
    print('MAPE: '+str(mape)) 