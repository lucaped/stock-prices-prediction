# Stock Price Prediction 

Analysis of the **stationarity** of the time series.\
Comparison of two methods (rolling mean and differencing) used to make a time series stationary.\
Development and study of **AutoRegressive Integrated Moving Average (ARIMA) model** for stock price prediction:

The performances of the ARIMA model are studied for a single and multiple time steps forecast.  

The dataset consists of the historical stock prices from 12/04/1996 till 11/10/2019 of [Altaba Inc.](https://www.altaba.com/investor-relations) that can be downoladed from [Yahoo finance](https://finance.yahoo.com/quote/AABA/).

Author: *L. Pedrelli*\
E-mail: luca.pedrelli1@gmail.com\
Project created: 07. August 2020

# Folder structure

| Folder/File name                  | Description   |
| :---                              | :---          |
| AABA.csv                          | Dataset of the hystorical stock prices from 12/04/1996 till 11/10/2019 of Altaba Inc.| 
| ARIMA_StockPricePrediction.ipynb  | Jupiter notebook containing the stationary analysis of the time series and the ARIMA model. It includes the code lines to eventually run the script on Colab GPUs. |
| ARIMA_StockPricePrediction.py     | Python file containing the stationary analysis of the time series and the ARIMA model. |
| function.py                       | Auxiliary functions |
| images                            | Contains conceptual figures |

#  Analysis and future forecast of time series

### Stationary time series

A time series is refered to as stationary if the statistical properties of the time series do not change over time.\
Two methods for making the time series stationary have been analysed:
1) subtraction of the weighted rolling mean from the original dataset 
2) differencing

The [Augmented Dickey-Fuller (ADF) test](https://en.wikipedia.org/wiki/Augmented_Dickey–Fuller_test) is used to test the null hypothesis that a unit root is present in the time series.\
Interpretation of the ADF test results:\
ADF statistic: the more negative the more likely we are to reject the null hypothesis, *i.e.* we have a *stationary* dataset\
p-value > 5% (or 1%) the data has a unit root, _i.e._ it is *non-stationary*\
p-value < 5% (or 1%) the data does not have a unit root, _i.e._ it is *stationary*

**Note**: the differencing method is the one implemented in the ARIMA model.

<div align="center">
    <figure>
        <img src="images/Stationary_time_series.png"  width="100%" height="100%" class="center">
        <font size="2">
        <figcaption> <strong>Figure 1) </strong> Original dataset (left), weighted rolling mean subtracted from the original dataset (center), and differencing the original dataset (right). The values below each plot show the result from the ADF test. 
        </figcaption>
        </font>
    </figure>
</div>

### ARIMA model

The ARIMA model is a type of linear regression model that uses the past values of the time series to forecast its future values.\
The ARIMA model is composed by three steps:
1) AutoRegressive (AR): the next observation of the variable is a regression of the past observations.
2) Integrated (I): it is a differentiation step to make the time series more stationary. In this step the data values are replaced by the difference between the data values and the previous values.
3) Moving Average (MA): it incorporates the relation between the observation and the residual error from a moving average model.

The ARIMA model takes three order parameters (p, d and q) which are connected to the mentioned three steps:\
p is the order of the AR model, i.e. the number of lag observations\
d is the degree of differencing needed for stationarity, and\
q is the number of lagged forecast errors in the prediction equation. This is essentially the size of the “window” function over your time series data.

To identify the optimum set of p, q and d values, the [auto_arima](https://alkaline-ml.com/pmdarima/about.html) function is used. The auto_arima operates like a grid search and tries set of parameters to minimize the selected critierion (e.g. AIC, BIC, ...).

The ARIMA model is used to forecast the future stock prices one step ahead (**one step forecast**) and for multiple steps in the future (**multi step forecast**).\
In the **one step forecast**, the ARIMA model forecast the next day stock price using the actual historical stock prices.\
In the **multi step forecast**, the stock price forecast of the ARIMA model at time t is used to forecast the stock price at time t+1.\
The two types of forecasts are schematically shown in **Figure 2**. The predicted prices using the ARIMA model are shown in **Figure 3** for both the **one step forecast** (yellow) and **multi step forecast** (green).

<div align="center">
    <figure>
        <img src="images/One_and_Multi_step.png"  width="85%" height="85%" class="center">
        <font size="2">
        <figcaption> <strong>Figure 2) </strong> Schematic of the one step (left) and multi step (right) forecast of the stock prices. 
        </figcaption>
        </font>
    </figure>
    <figure>
        <img src="images/ARIMA_predictions_2.png"  width="65%" height="65%" class="center">
        <font size="2">
        <figcaption> <strong>Figure 3) </strong> ARIMA forecasts of the stock prices using one step (yellow) and multi step (green) forecast from the training set of the historical values (red). The ARIMA forecasts and the test set are highlighted in the plot on the right. The table reports the performance of the ARIMA model in terms of mean squared error (MSE), mean absolute error (MAE), and mean absolute percentage error (MAPE) for one step and multi step forecast.
        </figcaption>
        </font>
    </figure>
</div>

## Possible future steps
Prediction of the stock market prices using a Long Short-Term Memory (LSTM) model.

### LSTM model

The Long Short-Term Memory (LSTM) is a characteristic recurrent neural network (RNN) designed to learn long term dependencies in time series.\
Like a RNN, the LSTM architecture has a chain-like structure with repeating modules. Each module of the LSTM chain is characterized by four different neural network layers. The schematic of the LSTM chain is shown in Figure 3.\
Each module takes the input x<sub>t</sub> and outputs a value h<sub>t</sub>. The peculiarity of the LSTM module is the **cell state**: the horizontal line running through the LSTM module with input value C<sub>t-1</sub> and output value C<sub>t</sub>. The LSTM has the ability to add or remove information from the cell state using three *ad-hoc* gates:
- the **forget gate** layer (f<sub>t</sub>) is responsible to decide which information is going to be thrown away from the cell state,
- the **update gate** layer (i<sub>t</sub>) is responsible to add the new candidate information value to the cell state, and
- the **output gate** layer (o<sub>t</sub>) decides which parts of the cell state are going to be the output of the LSTM module.

<div align="center">
    <figure>
        <img src="images/LSTM_scheme.png"  width="70%" height="70%" class="center">
        <font size="2">
        <figcaption> <strong>Figure 3) </strong> Schematic of the LSTM module. Image adapted from [this post](http://colah.github.io/posts/2015-08-Understanding-LSTMs/).
        </figcaption>
        </font>
    </figure>
</div>

# Development and dependencies

The codes have been developped in Jupyter Notebook for fast prototyping. The corresponding Python files are also provided.

**Dependencies**

* 	numpy v1.18.5
* 	scikit-learn v0.20.3
* 	tensorflow v2.1.0
*   pmdarima v1.2.0
* 	keras v2.3.1
*   pandas v1.1.1
* 	matplotlib v3.3.1
* 	pickle

# References

- Understanding LSTM Networks, _Colah's blog_ ([link](http://colah.github.io/posts/2015-08-Understanding-LSTMs/))
- Predicting Stock Price with LSTM ([link](https://towardsdatascience.com/predicting-stock-price-with-lstm-13af86a74944))
- Stock Market Predictions with LSTM in Python ([link](https://www.datacamp.com/community/tutorials/lstm-python-stock-market))

# Disclaimer

This project has the scope to investigate time series in the context of stock prices using the ARIMA model. **Forecasting stock prices is not a trivial task and this project is not an investment recommendation or anything like that**.\
All functions in this toolbox were implemented with care and tested.
Nevertheless, they may contain errors or bugs, which may affect the outcome of your analysis.
We do not take responsibility for any harm coming from using this toolbox,
neither if it is caused by errors in the software nor if it is caused by its improper application.\
See licensing terms for details.\
Please email me for any bugs you find.
